<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peliculas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peliculas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'cartel')->fileInput() ?>
  
    <?php
    if(!is_null($model->cartel)){
        echo Html::img('@web/imgs/' . $model->cartel, [
        'alt' => 'My logo',
        'width'=>800,
        'class'=> 'img-responsive img-thumbnail'
        ]);
    }
    ?>
  
    

    <?= $form->field($model, 'duracion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
