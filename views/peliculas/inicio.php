<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peliculas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peliculas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'titulo',
            [
            'label'=>'Cartel',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/" . $data->cartel;
                return Html::img($url,[
                    'width'=>'250',
                    'alt'=>'yii']); 
            }
            ],
            // añadir boton mas...       
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a(
                        'Mas...', 
                        ['peliculas/ver','id'=>$model->id],
                        ['class'=>"btn btn-primary"]
                    );
                },
	        ],
            ],
        ],
    ]); ?>


</div>
