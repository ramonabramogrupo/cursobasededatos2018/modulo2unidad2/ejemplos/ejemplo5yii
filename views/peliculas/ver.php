<?php
    use yii\helpers\Html;
?>
<div class="jumbotron">
<h1><?= $model->titulo ?></h1>
</div>
<div class="row">
    <div class="col-sm-4">
<?= 
    Html::img('@web/imgs/' . $model->cartel, [
    'alt' => 'My logo',
    'width'=>800,
    'class'=> 'img-responsive img-thumbnail'
    ]) 
?>
    </div>

<ul class="col-sm-8">
    <li>Descripcion: <?= $model->descripcion ?></li>
    <li>Año: <?= $model->year ?></li>
    <li>Duración: <?= $model->duracion ?></li>
</ul>
</div>
<div class="row">
    <div class="col-sm-12">
 <?= \yii\helpers\Html::a( 'Atras', Yii::$app->request->referrer,[
     'class'=>'btn btn-primary'
 ]);?>
    </div>
</div>